# gammaSPC
EPICS driver for Gamma Vacuum SPC/SPCe Ion Pump Controllers

Things are generically named SPCe, referring to the Ethernet model.  
This version just works with ethernet mode.

## How the PVs are read

The PVs following PVs are read every SCAN second, where SCAN is a macro 
defined at iocssh: Current, Pressure, Pump Voltagem, Messages from Panel, Is High
Voltag On, Communication Mode and Set Point information.

The other information are read just on ioc boot, and if they are changed by a
set PV.

## Know issues
* The Gamma Vaccum always return the character '>' before the answer for some
command, but sometimes this character came after the answer. Because of this 
sometimes some fields could receive an alarm (on a long test during 3 days, 
this happened 3 times).
* The Gamma vaccum is sensitive to EPICS broadcast packages, so it is 
recommended to have it in an isolated network. Otherwise the device can
get stuck and need a power cycle.
